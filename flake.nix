{
  description = "My nix-config for more ore less everything in my homelab";

  inputs = {
    # Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    # You can access packages and modules from different nixpkgs revs
    # at the same time. Here's an working example:
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    # Also see the 'unstable-packages' overlay at 'overlays/default.nix'.

    # Home manager
    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # Agenix secret management
    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";

    # Homeage secret management für home-manager
    homeage = {
      url = "github:jordanisaacs/homeage";
      # Optional
      inputs.nixpkgs.follows = "nixpkgs";
    };

    hardware.url = "github:nixos/nixos-hardware";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Impermanence
    impermanence = { url = "github:nix-community/impermanence"; };

    # Shameless plug: looking for a way to nixify your themes and make
    # everything match nicely? Try nix-colors!
    # nix-colors.url = "github:misterio77/nix-colors";
  };

  outputs = { self, nixpkgs, home-manager, agenix, homeage, nixos-hardware
    , disko, impermanence, ... }@inputs:
    let
      inherit (self) outputs;
      forAllSystems = nixpkgs.lib.genAttrs [
        # "aarch64-linux"
        "x86_64-linux"
        # "aarch64-darwin"
        # "x86_64-darwin"
      ];
    in rec {
      # Your custom packages
      # Acessible through 'nix build', 'nix shell', etc
      packages = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in import ./pkgs { inherit pkgs; });
      # Devshell for bootstrapping
      # Acessible through 'nix develop' or 'nix-shell' (legacy)
      devShells = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in import ./shell.nix { inherit pkgs; });

      # Your custom packages and modifications, exported as overlays
      overlays = import ./overlays { inherit inputs; };
      # Reusable nixos modules you might want to export
      # These are usually stuff you would upstream into nixpkgs
      nixosModules = import ./modules/nixos;
      # Reusable home-manager modules you might want to export
      # These are usually stuff you would upstream into home-manager
      homeManagerModules = import ./modules/home-manager;

      # NixOS configuration entrypoint
      # Available through 'nixos-rebuild --flake .#your-hostname'
      nixosConfigurations = {
        # .iso image
        #  - nix build .#nixosConfigurations.{iso-console|iso-desktop}.config.system.build.inputsoImage
        iso = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          system = "x86_64-linux";
          modules = [ ./nixos/hosts/iso/configuration.nix ];
        };
        # Proxmox Template
        templatenixos = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            # > Our main nixos configuration file 
            ./nixos/hosts/templatenixos/configuration.nix
            inputs.disko.nixosModules.disko
            ./nixos/hosts/common/global
            # Custom config
            ./nixos/hosts/common/optional/optin-persistence.nix
            ./nixos/hosts/common/optional/server.nix
            # Users
            ./nixos/users/matthias
            # Agenix secrets
            {
              environment.systemPackages =
                [ agenix.packages.x86_64-linux.default ];
            }
          ];
        };
        # Headless
        homeassistant = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            # > Our main nixos configuration file <
            ./nixos/hosts/homeassistant/configuration.nix
            ./nixos/hosts/common/global
            # Custom config
            ./nixos/hosts/common/optional/server.nix
            # Users
            ./nixos/users/matthias
            # Agenix secrets
            {
              environment.systemPackages =
                [ agenix.packages.x86_64-linux.default ];
            }
          ];
        };
        nas = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            # > Our main nixos configuration file <
            ./nixos/hosts/nas/configuration.nix
            inputs.disko.nixosModules.disko
            ./nixos/hosts/common/global
            # Custom config
            ./nixos/hosts/common/optional/optin-persistence.nix
            ./nixos/hosts/common/optional/server.nix
            ./nixos/hosts/common/optional/btrfs.nix
            ./nixos/hosts/common/optional/btrbk-backuptarget.nix
            # Users
            ./nixos/users/matthias
            # Agenix secrets
            {
              environment.systemPackages =
                [ agenix.packages.x86_64-linux.default ];
            }
          ];
        };
        ripper = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs outputs; };
          modules = [
            # > Our main nixos configuration file <
            ./nixos/hosts/ripper/configuration.nix
            ./nixos/hosts/common/global
            # Custom config
            ./nixos/hosts/common/optional/desktop.nix
            ./nixos/hosts/common/optional/nvidia.nix
            ./nixos/hosts/common/optional/virtualization.nix
            # Users
            ./nixos/users/matthias
            # Agenix secrets
            {
              environment.systemPackages =
                [ agenix.packages.x86_64-linux.default ];
            }
          ];
        };
      };

      # Standalone home-manager configuration entrypoint
      # Available through 'home-manager --flake .#your-username@your-hostname'
      homeConfigurations = {
        "matthias@unspezifisch" = home-manager.lib.homeManagerConfiguration {
          pkgs =
            nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
          extraSpecialArgs = { inherit inputs outputs; };
          modules = [
            # > Our main home-manager configuration file <
            ./home-manager/home.nix
          ];
        };
        "matthias@ripper" = home-manager.lib.homeManagerConfiguration {
          pkgs =
            nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
          extraSpecialArgs = { inherit inputs outputs; };
          modules = [
            ./home-manager/home.nix
            ./home-manager/gaming.nix
            ./home-manager/desktop.nix
          ];
        };
      };
    };
}
