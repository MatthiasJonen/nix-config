# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/profiles/qemu-guest.nix") ];

  boot.initrd.availableKernelModules =
    [ "ata_piix" "uhci_hcd" "virtio_pci" "virtio_blk" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  services.qemuGuest.enable = true;
  services.spice-vdagentd.enable = true;

  fileSystems."/" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    fsType = "btrfs";
    options = [
      "subvol=root"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  # Impermanence per btrfs snapshot herbeiführen
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /btrfs_tmp
    mount /dev/disk/by-partlabel/disk-rootdisk-root /btrfs_tmp
    if [[ -e /btrfs_tmp/root ]]; then
        mkdir -p /btrfs_tmp/old_roots
        timestamp=$(date --date="@$(stat -c %Y /btrfs_tmp/root)" "+%Y-%m-%-d_%H:%M:%S")
        mv /btrfs_tmp/root "/btrfs_tmp/old_roots/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/btrfs_tmp/$i"
        done
        btrfs subvolume delete "$1"
    }

    for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +3); do  # die Zahl nach + ist die Anzahl an Tagen Snapshot aufheben
        delete_subvolume_recursively "$i"
    done

    btrfs subvolume create /btrfs_tmp/root
    umount /btrfs_tmp
  '';

  # Einfachste Möglichkeit, bei SSH und Neuinstallation keine Extraschritte für SSH Zugriff gehen zu müssen
  fileSystems."/etc/ssh" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    depends = [ "/persist" ];
    neededForBoot = true;
    fsType = "btrfs";
    options = [
      "subvol=ssh"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  fileSystems."/persist" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    neededForBoot = true;
    fsType = "btrfs";
    options = [
      "subvol=persist"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    fsType = "btrfs";
    options = [
      "subvol=home"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  fileSystems."/nix" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    fsType = "btrfs";
    options = [
      "subvol=nix"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  fileSystems."/snapshot" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-root";
    fsType = "btrfs";
    options = [
      "subvol=snapshot"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-partlabel/disk-rootdisk-ESP";
    fsType = "vfat";
  };

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
