{
  disko.devices = {
    disk = {
      rootdisk = {
        type = "disk";
        device = "/dev/vda";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              size = "1M";
              type = "EF02";
            };
            ESP = {
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                subvolumes = {
                  "/home" = {
                    mountOptions = [ "compress=zstd" ];
                    mountpoint = "/home";
                  };
                  "/nix" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/nix";
                  };
                  "/persist" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/persist";
                  };
                  "/ssh" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/etc/ssh";
                  };
                  "/snapshot" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/snapshot";
                  };
                };
              };
            };
          };
        };
      };
      datadisk = {
        type = "disk";
        device = "/dev/vdb";
        content = {
          type = "gpt";
          partitions = {
            data = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ];
                subvolumes = {
                  "/data" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/data";
                  };
                  "/datasnapshot" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/data/snapshot";
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
