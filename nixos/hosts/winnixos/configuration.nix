# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "winnixos"; # Define your hostname.

  # HyperV Spezifisches
  # services.xserver = {
  # modules = [ pkgs.xorg.xf86videofbdev ];
  # videoDrivers = [ "hyperv_fb" ];
  # };
  users.users.gdm = { extraGroups = [ "video" ]; };
  users.users.matthias = { extraGroups = [ "video" ]; };
}
