{ config, pkgs, ... }:

{
  services.home-assistant = {
    enable = true;
    package = (pkgs.home-assistant.override {
      extraPackages = py: with py; [ psycopg2 ];
    }).overrideAttrs (oldAttrs: { doInstallCheck = false; });
    extraComponents =
      [ "esphome" "met" "radio_browser" "mqtt" "tasmota" "fritzbox" ];
    customComponents = with pkgs; [ ha-fusionsolar ha-ewelink ];
    openFirewall = true;
    config = {
      default_config = { };
      recorder.db_url = "postgresql://@/hass";
      homeassistant = {
        name = "Daheim";
        latitude = 47.759231;
        longitude = 16.44451;
        elevation = 198;
        unit_system = "metric";
        currency = "EUR";
        country = "AT";
        time_zone = "Europe/Vienna";
      };
      "automation ui" = "!include automations.yaml";
      mqtt = {
        sensor = {
          name = "Zisterne Füllstand";
          unique_id = "zisternefüllstand";
          state_topic = "zisterne/füllstand_in_prozent";
          unit_of_measurement = "%";
          state_class = "measurement";
        };
      };
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "hass" ];
    ensureUsers = [{
      name = "hass";
      ensureDBOwnership = true;
    }];
  };

  #mqtt
  age.secrets.mqtt-homeassistant.file =
    ../../../secrets/homeassistant/mqtt-homeassistant.age;

  services.mosquitto = {
    enable = true;
    listeners = [{
      users = {
        homeassistant = {
          acl = [ "readwrite #" ];
          passwordFile = config.age.secrets.mqtt-homeassistant.path;
        };
      };
      port = 1883;
    }];
  };
  networking.firewall.allowedTCPPorts = [ 1883 ];
}
