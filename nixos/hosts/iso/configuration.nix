{ pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix") ];

  # Packages, die es braucht
  environment.systemPackages = [ pkgs.rsync ];
  # Enable SSH in the boot process.
  systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJvhq4SNx2pe/8DV/EOa1OQqXm6hlND/gprD/z7zU/wD matthias@ripper"
  ];
}
