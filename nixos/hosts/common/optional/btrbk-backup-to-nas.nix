{ config, pkgs, ... }:

{
  # Credits:
  # https://github.com/suderman/nixos/blob/15969aa685d71cdb34699e323462c767a6f003c9/modules/btrbk/default.nix
  # Voraussetzung für Remote Backups mit btrbk
  age.secrets.btrbkbackup = {
    file = ../../../../secrets/privatekey-btrbkbackup.age;
    owner = "btrbk";
    mode = "400";
  };

  services.btrbk = {
    extraPackages = [ pkgs.lz4 ];
    instances = {
      "application_data_nas" = {
        onCalendar = "00:15";
        settings = {
          preserve_day_of_week = "monday";
          preserve_hour_of_day = "23";
          ssh_user = "btrbk";
          ssh_identity = config.age.secrets.btrbkbackup.path;
          stream_compress = "lz4";
          snapshot_create = "onchange";
          snapshot_preserve_min = "all";
          target_preserve_min = "1d";
          target_preserve = "7d 4w 6m";
          volume = {
            "/srv" = {
              subvolume = "local";
              target = "ssh://nas:2222/snapshot/${config.networking.hostName}";
            };
          };
        };
      };
    };
  };
}
