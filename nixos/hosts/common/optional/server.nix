{ config, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    openFirewall = true;
    ports = [ 22 2222 ];
    settings = {
      PermitRootLogin = "without-password";
      PubkeyAuthentication = "yes";
      PasswordAuthentication = false;
      LogLevel = "VERBOSE";
    };
  };
  # Firewall für Tailscale Netzwerk öffnen
  networking.firewall.trustedInterfaces = [ "tailscale0" ];
  networking.firewall.allowedUDPPorts = [ config.services.tailscale.port ];

  # Configure tailscale ssh access
  services.tailscale = { extraUpFlags = [ "--ssh" ]; };
}
