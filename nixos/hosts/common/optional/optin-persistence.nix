# This file defines the "non-hardware dependent" part of opt-in persistence
# It imports impermanence, defines the basic persisted dirs, and ensures each
# users' home persist dir exists and has the right permissions
#
# It works even if / is tmpfs, btrfs snapshot, or even not ephemeral at all.
{ inputs, ... }: {
  imports = [ inputs.impermanence.nixosModules.impermanence ];

  environment.persistence =
    { # TODO: service spezifische Directories in jeweiliges nix file bauen
      "/persist" = {
        hideMounts = true;
        directories = [
          "/var/lib/systemd"
          "/var/lib/nixos"
          "/var/lib/NetworkManager"
          "/var/lib/tailscale"
          "/var/lib/docker"
          "/var/lib/netdata"
          "/var/lib/fail2ban"
          # "/var/log" # ich weiß nicht, ob ich logfiles über einen Reboot brauche
        ];
        files = [
          "/etc/machine-id"
          {
            file = "/root/.secrets/agenix-infrastruktur-key";
            parentDirectory = { mode = "u=rw,g=root,o=root"; };
          }
        ];
      };
    };
  programs.fuse.userAllowOther = true;

  # services.openssh.hostKeys = [{
  #   path = "/persist/etc/ssh/ssh_host_ed25519_key";
  #   type = "ed25519";
  # }];

  # Root backup Zugriff
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFFdPOQhbsXv3b6LcIHzrL3ElKT5jqC8hTKztCQnsxLf matthias@homelab"
  ];

  # User können nicht mehr mutable sein
  users.mutableUsers = false;
}
