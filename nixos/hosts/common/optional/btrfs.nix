{ pkgs, ... }:

{
  # BTRFS
  environment.systemPackages = with pkgs; [
    btrfs-progs
    btdu # Sampling disk usage profiler for btrfs
    bcache-tools # für hdd cache --> Performance
  ];

  # Lokale Snapshots mit btrbk
  services.btrbk = {
    extraPackages = [ pkgs.lz4 ];
    instances."local" = {
      onCalendar = "*-*-* *:00:00";
      settings = {
        snapshot_preserve_min = "2d";
        snapshot_preserve = "48h 20d 10w 6m";
        snapshot_create = "onchange";
        snapshot_dir = "/snapshot";
        volume = {
          "/" = {
            subvolume = {
              persist = { };
              home = { };
            };
          };
        };
      };
    };
  };
}
