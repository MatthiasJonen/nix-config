{ pkgs, ... }:

{
  services.btrbk.sshAccess = [{
    key =
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA0PhO8pV/KrBw53zlsRyppt5r4Dc5RkfyrvhwKzNdIC btrbk@backup";
    roles = [ "info" "source" "target" "delete" "snapshot" "send" "receive" ];
  }];
}
