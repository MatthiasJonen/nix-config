{ config, pkgs, ... }:

{
  # Netdata monitoring
  services.netdata = {
    enable = true;
    enableAnalyticsReporting = true;
  };
}
