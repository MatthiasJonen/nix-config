{ inputs, ... }:

{
  imports = [ inputs.agenix.nixosModules.default ];

  age.identityPaths = [
    "/persist/root/.secrets/agenix-infrastruktur-key"
    "/root/.secrets/agenix-infrastruktur-key"
    "/home/matthias/.ssh/agenixdev"
  ];
}
