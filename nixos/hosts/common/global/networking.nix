{ pkgs, config, ... }:

{
  # secrets
  age.secrets.authkey-tailscale.file =
    ../../../../secrets/authkey-tailscale.age;

  # Enable networking
  networking.networkmanager.enable = true;

  # Firewall aktivieren
  networking.firewall.enable = true;

  # Configure Tailscale
  # make the tailscale command usable to users
  environment.systemPackages = [ pkgs.tailscale ];

  # enable the tailscale service
  services.tailscale = {
    enable = true;
    authKeyFile = config.age.secrets.authkey-tailscale.path;
  };
  # Firewall setting for tailscale
  networking.firewall.checkReversePath = "loose";

}
