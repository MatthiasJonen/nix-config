{ ... }:

{
  # fail2ban
  services.fail2ban = {
    enable = true;
    jails = {
      apache-nohome-iptables = ''
        # Block an IP address if it accesses a non-existent
        # home directory more than 5 times in 10 minutes,
        # since that indicates that it's scanning.
        filter   = apache-nohome
        action   = iptables-multiport[name=HTTP, port="http,https"]
        logpath  = /var/log/httpd/error_log*
        backend = auto
        findtime = 600
        bantime  = 600
        maxretry = 5
      '';
      dovecot = ''
        # block IPs which failed to log-in
        # aggressive mode add blocking for aborted connections
        enabled = true
        filter = dovecot[mode=aggressive]
        maxretry = 3
      '';
    };
    maxretry = 3;
    banaction = "iptables-multiport";
    banaction-allports = "iptables-allport";
    bantime-increment = {
      enable = true;
      rndtime = "10m";
      overalljails = true;
      multipliers = "2 4 16 128";
      maxtime = "96h";
    };
  };
  security.sudo = {
    enable = true;
    extraRules = [
      # Erlaube Commands ohne Passwort
      {
        users = [ "matthias" ];
        commands = [
          {
            command = "/run/current-system/sw/bin/nixos-rebuild";
            options = [ "SETENV" "NOPASSWD" ];
          }
          {
            command = "/home/matthias/.nix-profile/bin/nix-config";
            options = [ "SETENV" "NOPASSWD" ];
          }
          {
            command = "/run/current-system/sw/bin/rsync";
            options = [ "SETENV" "NOPASSWD" ];
          }
          {
            command = "/run/current-system/sw/bin/btrbk";
            options = [ "SETENV" "NOPASSWD" ];
          }
        ];
      }
    ];
  };
}
