{ config, pkgs, ... }:

# Temporäre Einstellungen, die Bugs fixen sollen
{
  #TODO: NetworkManager-wait-online.service is broken prüfen, ob temporärer Bugfix gelöscht werden kann
  # https://github.com/NixOS/nixpkgs/issues/59603
  # https://github.com/NixOS/nixpkgs/issues/180175
  systemd.services.NetworkManager-wait-online.enable = false;
}
