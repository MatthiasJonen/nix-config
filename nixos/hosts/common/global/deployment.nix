{ config, pkgs, ... }:

{
  # nur auf aktuelle Version wechseln, wenn es eine neue Version in main gibt
  systemd.services.deployment = {
    description =
      "Automatisches Wechseln auf neue nix-config Version, wenn main neuen commit hat";
    wants = [ "multi-user.target" ];
    startAt = [ "*:0/10" ]; # alle 10 min
    serviceConfig.Type = "simple";
    path = with pkgs; [ git nixos-rebuild ];
    script = with pkgs; ''
      # checken, ob es in Main neuen Commit gibt
      pushd /root/.config/nix-config
      git fetch
      UPSTREAM="@{u}"
      LOCAL=$(git rev-parse @)
      REMOTE=$(git rev-parse "$UPSTREAM")
      BASE=$(git merge-base @ "$UPSTREAM")

      if [ $LOCAL = $REMOTE ]; then
        echo "host ist aktuell" 
      elif [ $LOCAL = $BASE ]; then
        git pull
        nixos-rebuild switch --flake .#
      fi
      popd
    '';
  };
}
