# This file (and the global directory) holds config that I use on all hosts
{ outputs, pkgs, ... }: {
  imports = [
    ./agenix.nix
    ./bugfixes.nix
    ./deployment.nix
    ./monitoring.nix
    ./networking.nix
    ./nixos.nix
    ./security.nix
  ] ++ (builtins.attrValues outputs.nixosModules);
  age.identityPaths = [ "/root/.secrets/agenix-infrastruktur-key" ];

  # Basissachen, die kein eigenes .nix File benötigen
  virtualisation.docker = {
    enable = true;
    liveRestore = true;
    autoPrune = {
      enable = true;
      dates = "weekly";
    };
  };

  environment.systemPackages = with pkgs; [ docker-compose ];

  programs.fish.enable = true; # Warnhinweis nixos-rebuild
}
