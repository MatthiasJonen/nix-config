# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    (modulesPath + "/virtualisation/proxmox-image.nix")
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "pvenixos";

  # Enable networking
  networking.networkmanager.enable = true;

  nix = {
    settings = {
      # Enable flakes and new 'nix' command
      experimental-features = "nix-command flakes";
      # Deduplicate and optimize nix store
      auto-optimise-store = true;
    };
  };

  # Set your time zone.
  time.timeZone = "Europe/Vienna";
  # Select internationalisation properties.
  i18n.defaultLocale = "de_AT.utf8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_AT.utf8";
    LC_IDENTIFICATION = "de_AT.utf8";
    LC_MEASUREMENT = "de_AT.utf8";
    LC_MONETARY = "de_AT.utf8";
    LC_NAME = "de_AT.utf8";
    # LC_NUMERIC = "de_AT.utf8";
    LC_PAPER = "de_AT.utf8";
    LC_TELEPHONE = "de_AT.utf8";
    # LC_TIME = "de_AT.utf8";
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "at";
    xkbVariant = "nodeadkeys";
  };
  console.keyMap = "de-latin1";

  system.copySystemConfiguration = true;

  proxmox = {
    qemuConf = {
      name = "pvenixos";
      memory = 4096;
      cores = 2;
      additionalSpace = "30G";
      bios = "ovmf";
      bootSize = "512M";
    };
    qemuExtraConf = {
      cpu = "kvm64";
      onboot = 1;
    };
    filenameSuffix = "pvenixos-template";
    partitionTableType = "efi";
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    openFirewall = true;
    ports = [ 22 2222 ];
    settings = {
      PermitRootLogin = "without-password";
      PubkeyAuthentication = "yes";
      PasswordAuthentication = false;
      LogLevel = "VERBOSE";
    };
  };

  # User anlegen
  users.users.matthias = {
    isNormalUser = true;
    description = "Matthias Jonen";
    createHome = true;
    initialPassword = "initialPassword";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFFdPOQhbsXv3b6LcIHzrL3ElKT5jqC8hTKztCQnsxLf matthias@homelab"
    ];
  };

  # Home-manager
  systemd.tmpfiles.rules = [
    # Basis
    "d ${config.users.users.matthias.home}/.ssh 0700 matthias users"
    "d ${config.users.users.matthias.home}/.cache 0755 matthias users"
    "d /nix/var/nix/profiles/per-user/matthias"
  ];

  programs.fish.enable = true;

  system.stateVersion = "23.11"; # Did you read the comment?

}
