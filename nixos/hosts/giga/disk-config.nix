{
  disko.devices = {
    disk = {
      onboard = {
        type = "disk";
        device = "/dev/sda";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              priority = 1;
              name = "ESP";
              start = "1M";
              end = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                subvolumes = {
                  "/root" = {
                    mountOptions = [ "compress=zstd" ];
                    mountpoint = "/";
                  };
                  "/home" = {
                    mountOptions = [ "compress=zstd" ];
                    mountpoint = "/home";
                  };
                  "/nix" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/nix";
                  };
                  "/persist" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/persist";
                  };
                  "/ssh" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/etc/ssh";
                  };
                  "/snapshot" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/snapshot";
                  };
                  "/srvlocal" = {
                    mountOptions = [ "compress=zstd" "noatime" ];
                    mountpoint = "/srv/local";
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}

