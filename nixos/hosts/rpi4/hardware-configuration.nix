# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "usbhid" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/44444444-4444-4444-8888-888888888888";
    fsType = "ext4";
  };

  fileSystems."/mnt/backup" = {
    device = "/dev/disk/by-label/hddstorage";
    fsType = "btrfs";
    options = [
      "subvolid=256"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };
  fileSystems."/mnt/application_data" = {
    device = "/dev/disk/by-label/hddstorage";
    fsType = "btrfs";
    options = [
      "subvolid=257"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };
  fileSystems."/mnt/snapshots" = {
    device = "/dev/disk/by-label/hddstorage";
    fsType = "btrfs";
    options = [
      "subvolid=258"
      "defaults"
      "noatime"
      "autodefrag"
      "compress=zstd"
      "commit=120"
    ];
  };

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  networking.interfaces.end0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlan0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
}
