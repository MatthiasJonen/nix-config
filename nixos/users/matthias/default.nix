{ pkgs, config, ... }:
let
  ifTheyExist = groups:
    builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in {
  age.secrets.passwordfile-matthias.file =
    ../../../secrets/passwordfile-matthias.age;

  users.users.matthias = {
    isNormalUser = true;
    description = "Matthias Jonen";
    hashedPasswordFile = config.age.secrets.passwordfile-matthias.path;
    createHome = true;
    extraGroups = [ "networkmanager" "wheel" ] ++ ifTheyExist [ "docker" ];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFFdPOQhbsXv3b6LcIHzrL3ElKT5jqC8hTKztCQnsxLf matthias@homelab"
    ];
    packages = with pkgs; [ home-manager ];
  };

  # Für helix runtime
  systemd.tmpfiles.rules = [
    # Basis
    "d ${config.users.users.matthias.home}/.ssh 0700 matthias users"
    "d ${config.users.users.matthias.home}/.cache 0755 matthias users"
    # Helix
    "d ${config.users.users.matthias.home}/.cache/helix 0755 matthias users"
    "d ${config.users.users.matthias.home}/.cache/helix/runtime 0755 matthias users"
    # Home-manager
    "d /nix/var/nix/profiles/per-user/matthias"
  ];

  # agenix key for development of this repo and my user specific secrets
  age.secrets.privatekey-agenixdev-matthias = {
    file = ../../../secrets/privatekey-agenixdev-matthias.age;
    path = "/home/matthias/.ssh/agenixdev";
    mode = "400";
    owner = "matthias";
    group = "users";
    symlink = false;
  };

}
