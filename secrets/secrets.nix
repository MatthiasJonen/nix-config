let
  general =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGCNcl/VtT6K0oY4RWt0OqO56Tw+H+3jou5OXJNhERf4";
  agenixdev =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFEFHVE5OcpauSbGlxCLcmZDUmPm6292vDoyIFaBoaKm";
in {
  "passwordfile-matthias.age".publicKeys = [ general agenixdev ];
  "privatekey-agenixdev.age".publicKeys = [ general agenixdev ];
  "authkey-tailscale.age".publicKeys = [ general agenixdev ];
  "privatekey-btrbkbackup.age".publicKeys = [ general agenixdev ];
  # Homeage matthias
  "./matthias/config-ssh.age".publicKeys = [ agenixdev ];
  "./matthias/privatekey-gitlab.age".publicKeys = [ agenixdev ];
  "./matthias/privatekey-github.age".publicKeys = [ agenixdev ];
  "./matthias/privatekey-homelab.age".publicKeys = [ agenixdev ];
  "./matthias/privatekey-hetzner.age".publicKeys = [ agenixdev ];
  "./matthias/privatekey-isoroot.age".publicKeys = [ agenixdev ];
  "./matthias/accesstoken-gitlab.age".publicKeys = [ agenixdev ];
  # Hostspezifisch
  "./homeassistant/mqtt-homeassistant.age".publicKeys = [ general agenixdev ];
}
