{ lib, buildHomeAssistantComponent, fetchFromGitHub }:

buildHomeAssistantComponent rec {
  owner = "MatthiasJonen";
  domain = "fusion_solar";
  version = "v3.0.0";

  src = fetchFromGitHub {
    owner = "tijsverkoyen";
    repo = "HomeAssistant-FusionSolar";
    rev = "refs/tags/${version}";
    sha256 = "IqeB4ZvdCYP0YtwDU10cf7UJXg2R806Rnc73HnsP12o=";
  };

  dontBuild = true;

  meta = with lib; {
    changelog =
      "https://github.com/tijsverkoyen/HomeAssistant-FusionSolar/compare/v2.6.2...v3.0.0";
    description = "Integrate FusionSolar into your Home Assistant.";
    homepage = "https://github.com/tijsverkoyen/HomeAssistant-FusionSolar";
    # maintainers = "MatthiasJonen";
    license = licenses.mit;
  };
}
