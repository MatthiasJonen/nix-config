#!/usr/bin/env bash

# Funktionen definieren
# root
host_definieren() {
	hostsinconfig=$(nix flake show --no-warn-dirty | grep "NixOS configuration" | cut -d ':' -f 1 | sed 's/[^[:alnum:]]//g' | sed 's/321m0m1m//g' | sed 's/0m//g')
	host=$(grep -o "$HOSTNAME" <<<"$hostsinconfig")
	[ -z "$host" ] && echo "Host aus flake wählen:" && host=$(echo "$hostsinconfig" | tr " " "\n" | gum choose)
}

root_setup_pruefen() {
	root_setup_status="Status des nix-config Setups für root:\n"
	if [[ ! -f /root/.secrets/agenix-infrastruktur-key ]]; then
		root_setup_status+="- [ ] key zum secrets decrypten vorhanden\n"
		keyfehlt=true
	else
		root_setup_status+="- [x] key zum secrets decrypten vorhanden\n"
	fi
	if [[ ! -d /root/.config/nix-config ]]; then
		root_setup_status+="- [ ] nix-config Repo lokal vorhanden\n"
		repofehlt=true
	else
		root_setup_status+="- [x] nix-config Repo lokal vorhanden\n"
	fi
	if [[ ! -f /var/lib/netdata/cloud.d/cloud.conf ]]; then
		root_setup_status+="- [ ] host in netdata monitoring integriert\n"
		netdatafehlt=true
	else
		root_setup_status+="- [x] host in netdata monitoring integriert\n"
	fi
	# Zusammenfassende Darstellung oben durchgeführter Prüfungen
	echo -e "$root_setup_status" | gum format -t "markdown"
}
infrastruktur_key_laden() {
	if [[ $keyfehlt ]]; then
		if gum confirm "infrastrukur key aus bitwarden vault laden?" --default=true --affirmative "Ja" --negative "Nein"; then
			# shellcheck disable=SC2174
			mkdir -pm 700 /root/.secrets
			bw login
			bw get attachment agenix-infrastruktur-key --itemid 392d156e-20be-43d3-b355-af3001311b16 --output /root/.secrets/agenix-infrastruktur-key
			root_setup_pruefen
		fi
	fi
}

root_repo_klonen() {
	if [[ $repofehlt ]]; then
		if gum confirm "nix-config Repo lokal klonen?" --default=true --affirmative "Ja" --negative "Nein"; then
			git clone https://gitlab.com/MatthiasJonen/nix-config --single-branch /root/.config/nix-config
			root_setup_pruefen
		fi
	fi
}

nixos_rebuild() {
	if [[ ! $keyfehlt ]] || [[ ! $repofehlt ]]; then
		echo "Command für nixos-rebuild wählen"
		command=$(gum choose test switch update abbrechen)
		if [[ $command == "test" ]]; then
			pushd /root/.config/nix-config || exit
			nixos-rebuild test --flake .#"$host"
			popd || return
		elif [[ $command == "switch" ]]; then
			pushd /root/.config/nix-config || exit
			nixos-rebuild switch --flake .#"$host"
			popd || return
		elif [[ $command == "update" ]]; then
			pushd /root/.config/nix-config || exit
			git config user.email = "accounts.matthias@jonen.at"
			git config user.name = "MatthiasJonen"
			nix flake update --commit-lock-file
			popd || return
		fi
	fi
}

netdata_aktivieren() {
	if [[ $netdatafehlt ]]; then
		if gum confirm "host in netdata monitoring integrieren?" --default=true --affirmative "Ja" --negative "Nein"; then
			claimtoken=$(gum input --placeholder "Claim-Token aus netdata Dashboard angeben: ")
			echo "$claimtoken" >/var/lib/netdata/cloud.d/token
			nix-shell -p netdata --run "netdata-claim.sh"
			root_setup_pruefen
		fi
	fi
}

# user
user_setup_pruefen() {
	user_setup_status="Status des nix-config Setups für ${USER}:\n"
	if [[ ! -f $HOME/.ssh/agenixdev ]]; then
		user_setup_status+="- [ ] key zum secrets decrypten vorhanden\n"
		keyfehlt=true
	else
		user_setup_status+="- [x] key zum secrets decrypten vorhanden\n"
	fi
	if [[ ! -d $HOME/.config/nix-config ]]; then
		user_setup_status+="- [ ] nix-config Repo lokal vorhanden\n"
		repofehlt=true
	else
		user_setup_status+="- [x] nix-config Repo lokal vorhanden\n"
	fi
	# Zusammenfassende Darstellung oben durchgeführter Prüfungen
	echo -e "$user_setup_status" | gum format -t "markdown"
}
agenixdev_key_laden() {
	if [[ $keyfehlt ]]; then
		if gum confirm "agenixdev key aus bitwarden vault laden?" --default=true --affirmative "Ja" --negative "Nein"; then
			mkdir -p "$HOME"/.ssh
			chmod 700 "$HOME"/.ssh
			bw login
			bw get attachment agenixdev --itemid 0ea93215-79bb-4655-9f43-b061015e8441 --output "$HOME"/.ssh/agenixdev
			chmod 400 "$HOME"/.ssh/agenixdev
			user_setup_pruefen
		fi
	fi
}
repo_klonen() {
	if [[ $repofehlt ]]; then
		if gum confirm "Home-Manager Konfiguration durchführen und damit nix-config Repo lokal klonen?" --default=true --affirmative "Ja" --negative "Nein"; then
			if [[ ! -d /nix/var/nix/profiles/per-user/"$USER" ]]; then
		    sudo mkdir -p /nix/var/nix/profiles/per-user/"$USER" 
		  fi
			git clone https://gitlab.com/MatthiasJonen/nix-config /dev/shm/nix-config
			pushd /dev/shm/nix-config || exit
			hostinflake=$(grep -q "$USER@$HOSTNAME" flake.nix && echo "$HOSTNAME" || echo "unspezifisch")
			home-manager --extra-experimental-features "nix-command flakes" switch -b backup --flake .#"$USER"@"$hostinflake"
			popd || return
			rm -rf /dev/shm/nix-config
			git clone git@gitlab.com:MatthiasJonen/nix-config.git "$HOME"/.config/nix-config
			user_setup_pruefen
		fi
	fi
}
home_manager_switch() {
	if [[ ! $keyfehlt ]] || [[ ! $repofehlt ]]; then
		if gum confirm "home-manager switch durchführen?" --default=true --affirmative "Ja" --negative "Nein"; then
			pushd "$HOME"/.config/nix-config || exit
			hostinflake=$(grep -q "$USER@$HOSTNAME" flake.nix && echo "$HOSTNAME" || echo "unspezifisch")
			home-manager --extra-experimental-features "nix-command flakes" switch -b backup --flake .#"$USER"@"$hostinflake"
			popd || return
		fi
	fi
}

# tui
if [[ $EUID == 0 ]]; then
	host_definieren
	root_setup_pruefen
	infrastruktur_key_laden
	root_repo_klonen
	nixos_rebuild
	netdata_aktivieren
else
	user_setup_pruefen
	agenixdev_key_laden
	repo_klonen
	home_manager_switch
fi
