{ lib, stdenv, makeWrapper, gum, bitwarden-cli, coreutils, git, nix
, nixos-rebuild, openssh, home-manager, gnugrep, gnused, sudo, wget }:

# Referenz: https://github.com/Misterio77/nix-config/blob/main/pkgs/pass-wofi/default.nix

with lib;

stdenv.mkDerivation {
  name = "nix-config";
  version = "1.0";
  src = ./nix-config.sh;

  nativeBuildInputs = [ makeWrapper ];

  dontUnpack = true;
  dontBuild = true;
  dontConfigure = true;

  installPhase = ''
    install -Dm 0755 $src $out/bin/nix-config
    wrapProgram $out/bin/nix-config --set PATH "${
      makeBinPath [
        gum
        bitwarden-cli
        coreutils
        git
        nix
        nixos-rebuild
        openssh
        home-manager
        gnugrep
        gnused
        sudo
        wget
      ]
    }"
  '';

  meta = {
    description =
      "Setup Skript für dieses Repo, kann als user oder mit sudo aufgerufen werden und hat unterschiedliche Funktionen.";
    license = licenses.mit;
    platforms = platforms.all;
  };
}
