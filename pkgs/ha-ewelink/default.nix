{ lib, buildHomeAssistantComponent, fetchFromGitHub, pycryptodome }:

buildHomeAssistantComponent rec {
  owner = "AlexxIT";
  domain = "sonoff";
  version = "v3.6.0";

  src = fetchFromGitHub {
    owner = "AlexxIT";
    repo = "SonoffLAN";
    rev = "refs/tags/${version}";
    sha256 = "sha256-4fcGCc5cxdBNAEVGna991U2MLFztwiMsCWmgs9ko4kE=";
  };

  propagetedBuildInputs = [ pycryptodome ];

  # postPatch = ''
  #   substituteInPlace custom_components/sonoff/manifest.json --replace-fail 'pycryptodome>=3.6.6' 'pycryptodome==3.18.0'
  # '';

  dontBuild = true;
  dontCheckManifest = true;

  meta = with lib; {
    changelog = "https://github.com/AlexxIT/SonoffLAN/releases/tag/v3.6.0";
    description =
      "Home Assistant custom component for control Sonoff devices with eWeLink (original) firmware over LAN and/or Cloud.";
    homepage = "https://github.com/AlexxIT/SonoffLAN";
    # maintainers = "MatthiasJonen";
    license = licenses.mit;
  };
}
