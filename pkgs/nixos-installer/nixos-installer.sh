#!/usr/bin/env bash

# Temporäres Directory definieren, bei Exit automatisch aufgeräumt wird
temp=$(mktemp -d)
cleanup() {
	rm -rf "$temp"
}
trap cleanup EXIT

# Funktionen definieren
setup_pruefen() {
  setup_status="Status des Setups für Installer:\n"
	if [[ ! -f "$temp/persist/root/.secrets/agenix-infrastruktur-key" ]]; then
    setup_status+="- [ ] key zum secrets decrypten vorhanden\n"
    keyfehlt=true
  else
    setup_status+="- [x] key zum secrets decrypten vorhanden\n"
  fi
	# Zusammenfassende Darstellung oben durchgeführter Prüfungen
	echo -e "$setup_status" | gum format -t "markdown"
}
infrastruktur_key_laden() {
	if [[ $keyfehlt ]]; then
	  if gum confirm "Infrastrukur key aus bitwarden vault laden?" --default=true --affirmative "Ja" --negative "Nein"; then
	  	pushd "$temp" || exit
	  	install -d -m700 "$temp/persist/root/.secrets"
	  	bw login
	  	bw get attachment agenix-infrastruktur-key --itemid 392d156e-20be-43d3-b355-af3001311b16 --output "$temp/persist/root/.secrets/agenix-infrastruktur-key"
	  	chmod 600 "$temp/persist/root/.secrets/agenix-infrastruktur-key"
			popd || exit
      unset keyfehlt
		else
			exit
	  fi
  fi
}

installation_durchfuehren() {
	if [[ ! $keyfehlt ]]; then
	  if gum confirm "Installation durchführen?" --default=true --affirmative "Ja" --negative "Nein"; then
      host=$(gum input --placeholder "Host angeben")
      nix run github:nix-community/nixos-anywhere -- --extra-files "$temp" --flake .#"$host" root@nixos -i /home/matthias/.ssh/isoroot
		else
			exit
    fi
  fi
}
# tui
while true
  do
    setup_pruefen
    infrastruktur_key_laden
    installation_durchfuehren
  done
