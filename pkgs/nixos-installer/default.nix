{ lib, stdenv, makeWrapper, gum, bitwarden-cli, coreutils, nix, openssh }:

# Referenz: https://github.com/nix-community/nixos-anywhere/blob/main/docs/quickstart.md

with lib;

stdenv.mkDerivation {
  name = "nixos-installer";
  version = "1.0";
  src = ./nixos-installer.sh;

  nativeBuildInputs = [ makeWrapper ];

  dontUnpack = true;
  dontBuild = true;
  dontConfigure = true;

  installPhase = ''
    install -Dm 0755 $src $out/bin/nixos-installer
    wrapProgram $out/bin/nixos-installer --set PATH "${
      makeBinPath [ gum bitwarden-cli coreutils nix openssh ]
    }"
  '';

  meta = {
    description =
      "Installer Skript mit dem ich nixos-anywhere gut nutzen kann.";
    license = licenses.mit;
    platforms = platforms.all;
  };
}
