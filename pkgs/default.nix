# Custom packages, that can be defined similarly to ones from nixpkgs
# You can build them using 'nix build .#example' or (legacy) 'nix-build -A example'

{ pkgs ? (import ../nixpkgs.nix) { } }: {
  # example = pkgs.callPackage ./example { };
  nix-config = pkgs.callPackage ./nix-config { };
  nixos-installer = pkgs.callPackage ./nixos-installer { };
  ha-fusionsolar = pkgs.callPackage ./ha-fusionsolar { };
  ha-ewelink = pkgs.python311Packages.callPackage ./ha-ewelink { };
}
