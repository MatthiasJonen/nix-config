# nix-config

## Description
My nix(os) configuration for all the homelab infrastructure which needs to be reproducable.
Idea: Find a proper way to manage my workstations and servers in a reproducable way.
The infrastructure consists of:

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
### nix-config tool
Can be startet in differnt ways which result in differnt options in the tool:
On new host:
```sh
nix --extra-experimental-features "nix-command flakes" run gitlab:MatthiasJonen/nix-config#nix-config
```
On existing host:
```sh
nix-config
```
On existing host as root:
```sh
sudo nix-config
```
![nix-config](documentation/nix-config.gif)

## Installation
General tip: use the home-manager (user specific) configuration first and go from there.
### Build the installer image
Build it and you will find the iso in the results directory
```sh
nix build .#nixosConfigurations.iso.config.system.build.inputs.isoImage
```
### NixOS Host with installer image
SSH access to installer
```sh
ssh root@nixos -i ~/.ssh/isoroot  
```
use nixos-anywhere as normal user in git repo nix-config
```sh
nix run github:nix-community/nixos-anywhere -- --flake '.#<desired host config>' root@nixos -i ~/.ssh/isoroot
```
Create directory to copy infrastruktur-key to
```sh
ssh -t -i ~/.ssh/isoroot root@nixos 'mkdir /mnt/persist/root/.secrets'
```
Important: copy infrastruktur-key to /mnt/persist/root/.secrets/

On existing machine use nixos-anywhere:
Go as root to directory containing the infrastruktur-key and copy it to installer
```sh
scp -i /home/matthias/.ssh/isoroot agenix-infrastruktur-key root@nixos:/mnt/persist/root/.secrets/.
```
### NixOS Host
Install with the iso, add user matthias.
Run nix-config.
Copy configuration.nix and hardware-configuration.nix in a folder named after the hostname in this repo /nixos/hosts/
Delete most of the stuff of configuration.nix which is already covered by hosts/common
Add the new host in flake.nix
### Non-NixOS Host
Install home-manager and flakes.
Use nix-config tool.
## Usage
Some tips and statements for the usage of the configuration
### Restore btrbk from snapshots and backups
I like to rsync the data from the respective snapshot to the destination directory.
The reasioning behind this: I can mess up my restore without messing up my backups.

I show the example with my rpi4 remote backup configuration. Local is analog. The paths differ of course.
Find the snapshot/backup you want to restore from:
```sh
sudo btrbk -c /etc/btrbk/application_data_rpi4.conf list backup
```
cd to the desired target directory and rsync the file/directory you want recover (in this case home-assistant):
```sh
sudo rsync -av root@rpi4:/mnt/backup/application_data.20231125T2135/home-assistant .
```
![btrbk-restore](documentation/btrbk-restore.gif)

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Simple and documented config templates to help you get started with NixOS + home-manager + flakes. All the boilerplate you need!:\
[Misterio77](https://github.com/Misterio77) [nix-starter-configs](https://github.com/Misterio77/nix-starter-configs)\
Age-encrypted secrets for NixOS:\
[ryantm](https://github.com/ryantm) [agenix](https://github.com/ryantm/agenix)\
Tips for declarative fish configuration:\
[Ersei 'n Stuff](https://ersei.net/en/blog/its-nixin-time)

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
