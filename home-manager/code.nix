{ pkgs, inputs, ... }:

{
  imports = [ ];
  # Packages
  home.packages = with pkgs; [
    # Formater und Checker
    shellcheck # bash
    shfmt # bash
    # CLI Tools für Entwicklung
    manix # Nix Dokumentation
    nixfmt # Nix Formater
    glab # gitlab repo management
    gum # nettes cli tools framework
    vhs # gifs für Dokumentation erstellen
  ];

  # git
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userEmail = "accounts.matthias@jonen.at";
    userName = "MatthiasJonen";
    extraConfig = { pull.rebase = false; };
  };
  programs.lazygit = {
    enable = true;
    settings = {
      gui = {
        nerdFontsVersion = "3";
        theme = {
          activeBorderColor = [ "#89b4fa" "bold" ];
          inactiveBorderColor = [ "#a6adc8" ];
          optionsTextColor = [ "#89b4fa" ];
          selectedLineBgColor = [ "#313244" ];
          selectedRangeBgColor = [ "#313244" ];
          cherryPickedCommitBgColor = [ "#45475a" ];
          cherryPickedCommitFgColor = [ "#89b4fa" ];
          unstagedChangesColor = [ "#f38ba8" ];
          defaultFgColor = [ "#cdd6f4" ];
          searchingActiveBorderColor = [ "#f9e2af" ];
        };
      };
    };
  };

  # Helix
  programs.helix = {
    enable = true;
    package = pkgs.unstable.helix;
    extraPackages = with pkgs; [
      nodePackages.bash-language-server
      marksman
      nil
      nixd
    ];
    defaultEditor = true;
    settings = {
      theme = "catppuccin_mocha";
      editor = {
        true-color = true;
        line-number = "relative";
        lsp.display-messages = true;
        bufferline = "multiple";
        cursorline = true;
        cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "underline";
        };
      };
      keys.normal = {
        space.space = "file_picker";
        esc = [ "collapse_selection" "keep_primary_selection" ];
      };
    };
    languages = {
      language-server.nixd = { command = "nixd"; };
      language = [{
        name = "nix";
        language-servers = [ "nixd" "nil" ];
      }];
    };
  };
}
