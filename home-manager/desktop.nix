{ pkgs, ... }:

{
  # Packages
  home.packages = with pkgs; [ bitwarden ];
}
