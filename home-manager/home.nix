# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)

{ inputs, outputs, lib, config, pkgs, ... }: {
  # You can import other home-manager modules here
  imports = [
    # If you want to use modules your own flake exports (from modules/home-manager):
    # outputs.homeManagerModules.example

    # Or modules exported from other flakes (such as nix-colors):
    # inputs.nix-colors.homeManagerModules.default

    inputs.homeage.homeManagerModules.homeage

    # You can also split up your configuration and import pieces of it here:
    # ./nvim.nix
    ./code.nix
    ./shell.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = (_: true);
    };
  };

  home = {
    username = "matthias";
    homeDirectory = "/home/matthias";
  };

  # Homeage secret management (wird eigene configurationsdatei, wenn es funktioniert)
  homeage = {
    # identityPaths = [ "${config.xdg.configHome}/.ssh/agenixdev" ];
    identityPaths = [ "~/.ssh/agenixdev" ];
    installationType = "systemd";

    # ssh config
    file."sshconfig" = {
      source = ../secrets/${config.home.username}/config-ssh.age;
      copies = [ "${config.home.homeDirectory}/.ssh/config" ];
    };

    # gitlab
    file."privatekey-gitlab" = {
      source = ../secrets/${config.home.username}/privatekey-gitlab.age;
      copies = [ "${config.home.homeDirectory}/.ssh/gitlab" ];
    };
    file."accesstoken-gitlab" = {
      source = ../secrets/${config.home.username}/accesstoken-gitlab.age;
      copies = [ "${config.home.homeDirectory}/.ssh/accesstoken-gitlab" ];
    };

    # github
    file."privatekey-github" = {
      source = ../secrets/${config.home.username}/privatekey-github.age;
      copies = [ "${config.home.homeDirectory}/.ssh/github" ];
    };

    # hetzner
    file."privatekey-hetzner" = {
      source = ../secrets/${config.home.username}/privatekey-hetzner.age;
      copies = [ "${config.home.homeDirectory}/.ssh/hetzner" ];
    };

    # homelab
    file."privatekey-homelab" = {
      source = ../secrets/${config.home.username}/privatekey-homelab.age;
      copies = [ "${config.home.homeDirectory}/.ssh/homelab" ];
    };

    # installer iso
    file."privatekey-isoroot" = {
      source = ../secrets/${config.home.username}/privatekey-isoroot.age;
      copies = [ "${config.home.homeDirectory}/.ssh/isoroot" ];
    };
  };

  # Add stuff for your user as you see fit:
  # programs.neovim.enable = true;
  # home.packages = with pkgs; [ steam ];
  home.packages = with pkgs; [ nix-config ];

  # Enable home-manager and git
  programs.home-manager.enable = true;
  programs.git.enable = true;

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "22.11";
}
