{ pkgs, ... }:

{
  # Shell Konfiguration
  home.packages = with pkgs; [
    maple-mono-NF # Nerd font
    # Fish
    fd # Dependeny für fzf-fish
    fzf # Dependeny für fzf-fish
    bat # Dependeny für fzf-fish
    bat-extras.batgrep
    bat-extras.batman
    bat-extras.batpipe
    bat-extras.batwatch
    bat-extras.batdiff
    bat-extras.prettybat
    trash-cli # dependency für lf
    kubectl # FIXME: nur ergaenzt, weil mit Stand 20230724 in Tide der Command ausgeführt wird
    # tools
    tldr # bessere manpages
    tree # Optisch gute Übersicht über ein Verzeichnis
  ];

  # zellij Sessions im Terminal/Window Manager
  programs.zellij = {
    enable = true;
    package = pkgs.unstable.zellij;
    enableFishIntegration = false;
    settings = {
      default_shell = "fish";
      theme = "catppuccin-mocha";
    };
  };

  # Terminal file manager
  programs.lf = {
    enable = true;
    package = pkgs.unstable.lf;
    settings = {
      hidden = true;
      ignorecase = true;
      number = true;
      relativenumber = true;
      drawbox = true;
      preview = true;
      dirfirst = true;
    };
    keybindings = {
      D = "$trash $fx";
      U = "!du -sh";
    };
  };

  # Shell prompt
  programs.starship = { enable = true; };
  # Fish shell
  programs.fish = {
    enable = true;
    shellInit =
      "\n			source ~/.local/share/fish/home-manager_generated_completions/zellij.fish\n		";
    interactiveShellInit =
      "	set fish_greeting \n	set __done_notification_command '${pkgs.libnotify}/bin/notify-send \\$title \\$message'\n	export EDITOR=/home/matthias/.nix-profile/bin/hx\n";
    plugins = [
      {
        name = "fzf-fish";
        src = pkgs.fishPlugins.fzf-fish.src;
      }
      {
        name = "colored-man-pages";
        src = pkgs.fishPlugins.colored-man-pages.src;
      }
      {
        name = "sponge";
        src = pkgs.fishPlugins.sponge.src;
      } # History automatisch von typos befreien
      {
        name = "done";
        src = pkgs.fishPlugins.done.src;
      } # Notifier bei langen commands
      {
        name = "tide";
        src = pkgs.unstable.fishPlugins.tide.src;
      } # Shell prompt
    ];
    shellAbbrs = {
      # Nix home-manager
      nc = "nix-config";
      # Nix
      n = "cd ~/.config/nix-config";
      snc = "sudo nix-config";
      nixupdate =
        "pushd ~/.config/nix-config && nix flake update --commit-lock-file && popd";
      ns = ''
        nix --extra-experimental-features "nix-command flakes" run github:peterldowns/nix-search-cli -- --details'';
      nixformat = "find . -type f -name '*.nix' -print0 | xargs -0 nixfmt";
      nixcheck =
        "find . -type f -name 'flake.nix' -print0 | xargs -0 sh -c 'nix flake check'";
      # Bash skripting
      shformat =
        "find . -type f -name '*.sh' -print0 | xargs -0 shfmt --simplify --write";
      shcheck = "find . -type f -name '*.sh' -print0 | xargs -0 shellcheck";
      # git
      ga = "git add .";
      gs = "git status";
      gpush = "git push";
      gpull = "git pull";
      gdelmerged =
        "git branch --merged origin/main | grep -vw main | xargs git branch -d";
      # rm absichern
      rm = "trash";
      # Dokumentation
      keymaps =
        "${pkgs.gum}/bin/gum style --foreground 212 --border-foreground 212 --border double --align center --width 100 --margin \"1 2\" --padding \"2 4\" 'fzf:' 'ctrl+alt+(f File, r History, p Process, v Variable, l Git-Log, s Git-Status)'";
      # Zellij
      zellijbac = "bash (curl -L zellij.dev/launch | psub)";
    };
    functions = {
      gitignore = "curl -sL https://www.gitignore.io/api/$argv";

      lfcd = ''
        			  set tmp (mktemp)
                # `command` is needed in case `lfcd` is aliased to `lf`
                command lf -last-dir-path=$tmp $argv
                if test -f "$tmp"
                    set dir (cat $tmp)
                    rm -f $tmp
                    if test -d "$dir"
                        if test "$dir" != (pwd)
                            cd $dir
                        end
                    end
                end
        			'';
    };
  };
}
