{ pkgs, ... }:

{
  # Packages
  home.packages = with pkgs; [
    wineWowPackages.waylandFull
    winetricks
    lutris
    steam
    protonup-qt
  ];

  # Steam ist nicht über HomeManager konfiguriert, sondern über Desktop
}
